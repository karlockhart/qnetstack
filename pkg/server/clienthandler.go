package server

import (
	"encoding/json"
	"net"
	"time"

	"bitbucket.org/karlockhart/qnetstack/pkg/packet"
	"github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
)

// Client represents a connected client
type ClientHandler struct {
	id        uuid.UUID
	stopChan  chan bool
	sendChan  chan []byte
	sequence  uint64
	addr      *net.UDPAddr
	lastAcked uint64
	connected bool
	failures  uint16
}

func (h *ClientHandler) handle(n int, buf []byte) {
	packet := &packet.Packet{}
	err := json.Unmarshal(buf[:n], packet)

	if err != nil {
		log.WithFields(log.Fields{"unit": "server.clientHandler"}).Error(err)
	}

	log.WithFields(log.Fields{"unit": "server.clientHandler"}).Info(packet.Version)

}

func (h *ClientHandler) Send(buf []byte) {
	h.sendChan <- buf
}

func (h *ClientHandler) Stop() {
	close(h.stopChan)
}

// NewClient represents a new connected client
func NewClientHandler(addr *net.UDPAddr) (handler *ClientHandler) {
	id := uuid.NewV1()
	handler = &ClientHandler{
		id:        id,
		sequence:  1,
		addr:      addr,
		lastAcked: 0,
		connected: false,
		stopChan:  make(chan bool),
		sendChan:  make(chan []byte),
		failures:  0,
	}

	go func() {
		conn, err := net.DialUDP("udp", nil, addr)
		if err != nil {

		}
		defer conn.Close()

		ticker := time.NewTicker(500 * time.Millisecond)
	loop:
		for {

			select {
			case <-handler.stopChan:
				log.WithFields(log.Fields{"unit": "server.clientHandler"}).Info("Stopping handler for ", addr)
				break loop
			case payload := <-handler.sendChan:
				_, err := conn.Write(payload)
				if err != nil {
					log.WithFields(log.Fields{"unit": "server.clientHandler"}).Error(err)
					close(handler.stopChan)
				}
			case <-ticker.C:

				if handler.failures > 10 {
					log.WithFields(log.Fields{"unit": "server.clientHandler"}).Info("Challenge failure for ", addr)
					close(handler.stopChan)
					continue loop
				}

				if handler.connected {
					log.WithFields(log.Fields{"unit": "server.clientHandler"}).Info("Send state to ", addr)
				} else {
					log.WithFields(log.Fields{"unit": "server.clientHandler"}).Info("Send challenge to ", addr)
					handler.failures++
				}
			}
		}
	}()
	log.WithFields(log.Fields{"unit": "server.clientHandler"}).Info("Created new client handler for ", addr)

	return
}
