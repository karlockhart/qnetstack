package server

import (
	"net"
	"os"
	"os/signal"
	"time"

	"bitbucket.org/karlockhart/qnetstack/pkg/common"

	log "github.com/sirupsen/logrus"
)

const version = "1.0.0"

// Server represents the server
type Server struct {
	clients      map[net.Addr]*ClientHandler
	shutdownChan chan bool
	state        *common.State
}

// NewServer creates a new server process
func NewServer() (s *Server) {
	handlers := make(map[net.Addr]*ClientHandler)
	s = &Server{
		clients:      handlers,
		shutdownChan: make(chan bool),
		state:        common.NewState(),
	}
	return
}

func (s *Server) listen() {
	addr := net.UDPAddr{
		Port: 12345,
		IP:   net.ParseIP("0.0.0.0"),
	}
	log.WithFields(log.Fields{"unit": "server"}).Info("Starting listener on: ", addr.String())

	conn, err := net.ListenUDP("udp", &addr)
	if err != nil {
		panic(err)
	}
	defer conn.Close()

loop:
	for {
		select {
		case <-s.shutdownChan:
			log.WithFields(log.Fields{"unit": "server"}).Info("Stopping listener")
			break loop
		default:
			conn.SetReadDeadline(time.Now().Add(time.Second * 1))
			buf := make([]byte, 2048)
			n, addr, err := conn.ReadFromUDP(buf)
			if err != nil {
				if e, ok := err.(net.Error); !ok || !e.Timeout() {
					log.WithFields(log.Fields{"unit": "server"}).Error(err)
				}

			}

			if n > 0 {
				go s.processData(n, buf, addr)
			}
		}
	}

}

func (s *Server) processData(n int, buf []byte, addr *net.UDPAddr) {

	log.WithFields(log.Fields{"unit": "server"}).Info(string(buf[0:n]), addr)
	if _, ok := s.clients[addr]; !ok {
		s.clients[addr] = NewClientHandler(addr)
	}
	s.clients[addr].handle(n, buf)
}

// Start the server
func (s *Server) Start() {
	log.WithFields(log.Fields{"unit": "server"}).Info("Server starting, version: ", version)
	go s.listen()
	sigInt := make(chan os.Signal, 1)
	clean := make(chan bool)

	signal.Notify(sigInt, os.Interrupt)
	go func() {
		for _ = range sigInt {
			log.WithFields(log.Fields{"unit": "server"}).Info("Shutdown requested")
			close(s.shutdownChan)
			clean <- true
		}
	}()
	<-clean
	log.WithFields(log.Fields{"unit": "server"}).Info("Cleanup done, shutdown completed")
}
