package packet

import "errors"

//
const (
	ChallengeRequest = 1 << iota
	Challenge
	ChallengeResponse
	Command
	FullState
	StateDelta
)

// Header for all packet types
type Header struct {
	PacketType int
	Sequence   uint64
	Ack        uint64
	CommandAck uint64
	Command    string
	ID         string
}

func NewHeader(packetType int, lastSequence uint64) (h *Header) {
	h = &Header{}
	h.PacketType = packetType
	h.Sequence = lastSequence + 1

	return
}

func (h *Header) AddAck(sequence uint64) {
	h.Ack = sequence
}

func (h *Header) AddCommand(cmd string) error {
	if h.PacketType != Command {
		return errors.New("Cannot add command to non-command packet")
	}
	h.Command = cmd
	return nil
}

func (h *Header) AddCmdAck() {

}
