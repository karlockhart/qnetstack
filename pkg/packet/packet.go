package packet

import (
	"encoding/json"

	"bitbucket.org/karlockhart/qnetstack/pkg/common"
)

const version = "1.0.0"

// Packet represents the smallest sendable element
type Packet struct {
	Version string
	Header  *Header
	State   *common.State
}

func NewChallengeRequest() (p *Packet) {
	p = &Packet{
		Version: version,
		Header:  NewHeader(ChallengeRequest, 0),
	}
	return

}
func NewChallenge() *Packet {
	return nil
}

// Encode a packet to send
func (p *Packet) Encode(state *common.State) ([]byte, error) {
	if p.State != nil && state != nil {
		p.State = state.GetState()
	}
	return json.Marshal(p)
}

func Decode(raw []byte) (p *Packet) {
	json.Unmarshal(raw, p)
	return
}
