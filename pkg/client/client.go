package client

import (
	"net"

	"bitbucket.org/karlockhart/qnetstack/pkg/common"
	"bitbucket.org/karlockhart/qnetstack/pkg/packet"
)

const version = "1.0.0"

type Client struct {
	state *common.State
}

func NewClient() (c *Client) {

	c = &Client{state: common.NewState()}
	return
}

func (c *Client) Start() {

	addr := &net.UDPAddr{
		Port: 12345,
		IP:   net.ParseIP("127.0.01"),
	}

	conn, err := net.DialUDP("udp", nil, addr)
	if err != nil {

	}
	defer conn.Close()

	packet, _ := packet.NewChallengeRequest().Encode(nil)

	_, err = conn.Write(packet)
	if err != nil {

	}
}
