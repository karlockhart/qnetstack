package common

import (
	"errors"
	"math"
)

var epsilon = math.Nextafter(1.0, 2.0) - 1.0

// FloatEquals compares two floats for pseudo-equalitys
func FloatEquals(l, r float64) bool {
	return ((l-r) < epsilon && (r-l) < epsilon)
}

// State contains a state
type State struct {
	Integers map[string]int
	Strings  map[string]string
	Floats   map[string]float64
}

// NewState creates a new empty state
func NewState() (state *State) {
	i := make(map[string]int)
	s := make(map[string]string)
	f := make(map[string]float64)

	state = &State{
		Integers: i,
		Strings:  s,
		Floats:   f,
	}

	return
}

// Copy copies the state to a new state
func (s *State) Copy() (state *State) {
	state = NewState()
	for k, v := range s.Integers {
		state.SetInt(k, v)
	}

	for k, v := range s.Floats {
		state.SetFloat(k, v)
	}

	for k, v := range s.Strings {
		state.SetString(k, v)
	}

	return
}

// SetInt sets key k to value v
func (s *State) SetInt(k string, v int) {
	s.Integers[k] = v
}

// GetInt returns the value associated with a key
func (s *State) GetInt(k string) (int, error) {
	v, ok := s.Integers[k]
	if !ok {
		return 0, errors.New("key doesn't exit")
	}
	return v, nil
}

// SetFloat sets key k to value v
func (s *State) SetFloat(k string, v float64) {
	s.Floats[k] = v
}

// GetFloat returns the value associated with a key
func (s *State) GetFloat(k string) (float64, error) {
	v, ok := s.Floats[k]
	if !ok {
		return 0.0, errors.New("key doesn't exit")
	}
	return v, nil
}

func (s *State) getInts() map[string]int {
	return s.Integers
}

func (s *State) getFloats() map[string]float64 {
	return s.Floats
}

func (s *State) getStrings() map[string]string {
	return s.Strings
}

// SetString sets key k to value v
func (s *State) SetString(k string, v string) {
	s.Strings[k] = v
}

// GetString returns the value associated with a key
func (s *State) GetString(k string) (string, error) {
	v, ok := s.Strings[k]
	if !ok {
		return "", errors.New("key doesn't exit")
	}
	return v, nil
}

// Compress delta compresses the state with a previous state
func (s *State) Compress(as *State) (state *State) {
	state = NewState()
	for k, v := range s.Integers {
		sv, err := as.GetInt(k)

		if err != nil {
			// if the integer is new just add it
			state.SetInt(k, v)
			continue
		}

		if sv != v {
			state.SetInt(k, v-sv)
		}
	}

	for k, v := range s.Floats {
		sv, err := as.GetFloat(k)

		if err != nil {
			// if the float is new just add it
			state.SetFloat(k, v)
			continue
		}
		if !FloatEquals(sv, v) {
			state.SetFloat(k, v-sv)
		}
	}

	for k, v := range s.Strings {
		sv, err := as.GetString(k)

		if err != nil || sv != v {
			// if the string is new or different just add it
			state.SetString(k, v)
		}
	}

	return
}

// DecompressState takes a state and a delta compressed state and returns the new state
func DecompressState(as *State, dcs *State) (state *State) {
	state = as.Copy()
	for k, v := range dcs.Integers {
		dv, err := as.GetInt(k)

		if err != nil {
			state.SetInt(k, v)
			continue
		}
		state.SetInt(k, v+dv)
	}

	for k, v := range dcs.Floats {
		dv, err := as.GetFloat(k)

		if err != nil {
			state.SetFloat(k, v)
			continue
		}
		state.SetFloat(k, v+dv)
	}

	for k, v := range dcs.Strings {
		sv, err := as.GetString(k)

		if err != nil {
			state.SetString(k, v)
			continue
		}
		state.SetString(k, sv)
	}

	return
}

// GetState returns a non-updated copy of the current state
func (s *State) GetState() *State {
	return s.Copy()
}
