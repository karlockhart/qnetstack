package main

import (
	"fmt"

	"bitbucket.org/karlockhart/qnetstack/pkg/client"
	"bitbucket.org/karlockhart/qnetstack/pkg/server"
	docopt "github.com/docopt/docopt-go"
	log "github.com/sirupsen/logrus"
)

func main() {
	log.WithFields(log.Fields{"unit": "launcher"}).Info("Launcher starting")
	usage := `QNet.

		Usage:
		  qnet start (server|client)`
	arguments, _ := docopt.ParseDoc(usage)
	fmt.Println(arguments)
	if arguments["server"] == true {
		server := server.NewServer()
		server.Start()
	} else if arguments["client"] == true {
		client := client.NewClient()
		client.Start()
	}

}
